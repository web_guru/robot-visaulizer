import * as React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Visualizer } from '../../index';

test('Renders', async () => {
  const { getByRole } = render(<Visualizer rpm={10} />);
  expect(getByRole('canvas')).not.toBeFalsy();
});
