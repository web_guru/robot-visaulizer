import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  Scene,
  Engine,
  FreeCamera,
  Vector3,
  HemisphericLight,
  MeshBuilder,
  Mesh,
} from '@babylonjs/core';

export const SceneComponent = (props: any) => {
  const reactCanvas = useRef<null | HTMLCanvasElement>(null);
  const onRender = useRef(props.onRender);

  useEffect(() => {
    onRender.current = props.onRender;
  }, [props.onRender]);

  const {
    antialias,
    engineOptions,
    adaptToDeviceRatio,
    sceneOptions,
    onSceneReady,
    ...rest
  } = props;

  const [loaded, setLoaded] = useState(false);
  const [scene, setScene] = useState<Scene | null>(null);

  useEffect(() => {
    if (window) {
      const resize = () => {
        if (scene != null) {
          scene.getEngine().resize();
        }
      };
      window.addEventListener('resize', resize);

      return () => {
        window.removeEventListener('resize', resize);
      };
    }
    return () => {};
  }, [scene]);

  useEffect(() => {
    if (!loaded && reactCanvas.current) {
      setLoaded(true);
      const engine = new Engine(
        reactCanvas.current,
        antialias,
        engineOptions,
        adaptToDeviceRatio,
      );
      const scene = new Scene(engine, sceneOptions);
      setScene(scene);
      if (scene.isReady()) {
        props.onSceneReady(scene);
      } else {
        scene.onReadyObservable.addOnce(scene => props.onSceneReady(scene));
      }

      engine.runRenderLoop(() => {
        if (typeof onRender.current === 'function') {
          onRender.current(scene);
        }
        scene.render();
      });
    }

    return () => {
      if (scene !== null) {
        console.error('disposing of scene');
        scene.dispose();
        setLoaded(false);
      }
    };
  }, []);

  return (
    <canvas
      style={{ width: '100%', height: '100%' }}
      ref={reactCanvas}
      {...rest}
    />
  );
};

// For now I'm thinking the main way to control the visualizer will be declaratively through props.
// At some point, we may want to add a ref to an object that exposes an API.
export interface VisualizerProps {
  // exmaple prop that doesn't relate to robots
  rpm: number;
}

export const Visualizer = ({ rpm }: VisualizerProps) => {
  const box = useRef<Mesh | null>(null);

  const onSceneReady = (scene: Scene) => {
    // This creates and positions a free camera (non-mesh)
    const camera = new FreeCamera('camera1', new Vector3(0, 5, -10), scene);

    // This targets the camera to scene origin
    camera.setTarget(Vector3.Zero());

    const canvas = scene.getEngine().getRenderingCanvas();
    if (!canvas) {
      console.warn('scene does not have canvas');
      return;
    }

    // This attaches the camera to the canvas
    camera.attachControl(canvas, true);

    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    const light = new HemisphericLight('light', new Vector3(0, 1, 0), scene);

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;

    // Our built-in 'box' shape.
    box.current = MeshBuilder.CreateBox('box', { size: 2 }, scene);

    // Move the box upward 1/2 its height
    box.current.position.y = 1;

    // Our built-in 'ground' shape.
    MeshBuilder.CreateGround('ground', { width: 6, height: 6 }, scene);
  };

  /**
   * Will run on every frame render.  We are spinning the box on y-axis.
   */
  const onRender = useCallback(
    (scene: Scene) => {
      if (box.current) {
        const deltaTimeInMillis = scene.getEngine().getDeltaTime();

        box.current.rotation.y +=
          (rpm / 60) * Math.PI * 2 * (deltaTimeInMillis / 1000);
      }
    },
    [box, rpm],
  );

  return (
    <SceneComponent antialias onSceneReady={onSceneReady} onRender={onRender} />
  );
};
