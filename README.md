# Standard Bots Visualizer

This is our visualizer, written using babylonjs.

We use Storybook for testing the visualizer in isolation.

## Usage

To install dependencies

```
$ npm i
```

To build

```
$ npm run build
```

To run tests

```
$ npm run test
```

To run Storybook

```
$ npm run storybook
```

## Goals

An initial goal might be something like:

```typescript,tsx
<Visualizer mode="armPosition" jointAngles={[0, 0, 0, 0, 0, 0]} />
```

which would show the robot straight up from a fairly distant location.

However, we might want a different style of API:

```typescript,tsx
type JointMoment = {
  position: number;
  velocity: number;
  acceleration: number;
}

type Moment = {
  timestamp: number;
  joints: Array<JointMoment>;
}

type Motion = {
  moments: Array<Moment>;
}

interface VisualizerAPI {
  // example API method call
  visualizeMotion(motion: Motion): Promise<void>;
}

const visualizer = useRef<VisualizerAPI | null>(null);

const visualizeMotion = useCallback(() => {
  if (!visualizer.current) {
    alert('visualizer not loaded yet');
    return;
  }

  const motion = createMotion();

  visualizer.current.visualizeMotion(motion);
}, []);


<Visualizer apiRef={visualizerRef} />
<button onClick={visualizeMotion} />
```
